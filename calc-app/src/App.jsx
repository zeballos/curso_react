/* eslint no-eval: 0*/
//importacion
import React, { useState} from 'react'
import words from 'lodash.words';
import Result from './components/Result';
import MathOperations from './components/MathOperations';
import Functions from './components/Functions';
import Numbers from './components/Numbers';
import './App.css';


//generacion del componente
const App = () =>{
   const [stack,setStack] = useState("")
   
   const items = words(stack,/[^-^+^*^/]+/g)
   const value= items.length > 0 ? items[items.length-1]:0;
   
    console.log("renderizacion de app")
    
    return  (<main className='react-calculator'>

        <Result value={value}/>
        <Numbers 
            onClickNumbers= { number =>{ 
                setStack(`${stack}${number}`)
            }}
        /> 
        <Functions
            onContentClear={() => {
                setStack('0')
            }}
            onDelete={()=>{
                if(stack.length > 0 ){
                    const newStack = stack.substring(0,stack.length - 1)
                    setStack(newStack)
                }
            }}
        />
        <MathOperations 
                onClickOperation={operation=>{  setStack(`${stack}${operation}` ) }}
                onClickEqual={equal=>{ setStack(eval(stack)) }}
        />
    </main>);
}

//exportacion del componente
export default App;