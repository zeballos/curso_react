import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

const numbers = [7,8,9,4,5,6,1,2,3,0]
const renderButtons = onClickNumbers => {
    
    const renderButton = number => (
        <Button  key={number} text={number.toString()} clickHandler={onClickNumbers} />
    )
    return  numbers.map(renderButton)
}

const Numbers = ({ onClickNumbers }) => {
    return (
        <div className="numbers" >
            { 
                renderButtons(onClickNumbers)
            }       
        </div>
    );
}

Numbers.propTypes = {
    onClickNumbers:PropTypes.func.isRequired
}

export default Numbers;